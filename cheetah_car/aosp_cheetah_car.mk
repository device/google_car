#
# Copyright 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DEVICE_IS_64BIT_ONLY := true

PRODUCT_PACKAGE_OVERLAYS += device/google_car/cheetah_car/overlay

PRODUCT_COPY_FILES += \
    device/google_car/cheetah_car/displayconfig/display_id_4619827677550801152.xml:$(TARGET_COPY_OUT_VENDOR)/etc/displayconfig/display_id_4619827677550801152.xml

$(call inherit-product, device/google_car/common/pre_google_car.mk)
$(call inherit-product, device/google_car/cheetah_car/device-cheetah-car.mk)
$(call inherit-product-if-exists, vendor/google_devices/pantah/proprietary/cheetah/device-vendor-cheetah.mk)
$(call inherit-product, device/google_car/common/post_google_car.mk)

# Disable production validation checks to fix build error from cheetah.scl
PRODUCT_VALIDATION_CHECKS :=

PRODUCT_NAME := aosp_cheetah_car
PRODUCT_DEVICE := cheetah
PRODUCT_MODEL := AOSP on Cheetah
PRODUCT_BRAND := Android
PRODUCT_MANUFACTURER := Google
